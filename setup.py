import os
import subprocess
from setuptools import setup, find_packages
from setuptools.command.install import install

# Define the root directory for proto files
proto_root = "src/proto"


class CustomInstallCommand(install):
    def run(self):
        # Walk the directory
        for root, dirs, files in os.walk(proto_root):
            for file in files:
                if file.endswith(".proto"):
                    print("Compiling proto file: " + file)

                    # Compile the proto file into the relative directory under proto_root
                    command = f"protoc --proto_path={root} --python_out={proto_root} {os.path.join(root, file)}"
                    subprocess.call(command, shell=True)

        # python install code
        install.run(self)


setup(
    name='Manga Plus Downloader',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'click',
        'requests',
        'tabulate',
        'tqdm',
        'protobuf',
    ],
    entry_points='''
        [console_scripts]
        mp-dl=src.mp_dl:MangaPlusDownloader
    ''',
    cmdclass={
        'install': CustomInstallCommand,
    },
)

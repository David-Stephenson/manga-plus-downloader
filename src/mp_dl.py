import click
from tabulate import tabulate
from src.utils.config import read_config, write_config
from src.utils.find_series import find_series
from src.utils.find_chapters import find_chapters
from src.utils.downloader import downloader


@click.command()
@click.option('--list', is_flag=True, help="List all mangas or chapters. (Use --manga to list chapters of a manga) (Default: list mangas)")
@click.option('--download', is_flag=True, help="Indicates a download action.")
@click.option('--manga', type=int,  help="The ID of the maga to download.")
@click.option('--chapter', type=str, default='latest', help="The ID of the chapter to download. (ID, latest, all) (Default: latest)")
@click.option('--quality', type=str, default='super', help="The quality of the images to download. (super, high, low) (Default: super)")
def MangaPlusDownloader(list, download, manga, chapter, quality):
    read_config()
    """Download manga from Manga Plus."""
    if list:
        if manga:
            chapters_list = find_chapters(manga)
            print(tabulate(chapters_list, headers="keys"))
        else:
            maga_list = find_series()
            print(tabulate(maga_list, headers="keys"))
        return

    # Download
    elif download:
        chapter = chapter.lower()
        if (manga == None or chapter == None):
            print(
                "Error: You need to specify a manga and a chapter to download. Use --help for more information.")
            return

        downloader(manga, chapter, quality)
        return
    else:
        print(
            "Manga Plus Downloader, Use --help for more information.")
        return


if __name__ == "__main__":
    MangaPlusDownloader()

import os
import json

from src.utils import create_secret

config_file = os.path.join(os.path.expanduser('~'), ".manga_plus_config")

data = {}


def read_config():
    # Check if config file exists
    while not os.path.isfile(config_file):
        try:
            print("Config file not found, creating one...")
            write_config()
        except Exception as e:
            print(e)
            break
    with open(config_file, "r") as f:
        data = json.load(f)
        os.environ["MANGA_PLUS_SECRET"] = data["secret"]
        # print(f"Loaded Secret: {os.environ['MANGA_PLUS_SECRET']}")


def write_config():
    # Write config file
    data["secret"] = create_secret.get_secret()
    with open(config_file, "w") as f:
        json.dump(data, f)

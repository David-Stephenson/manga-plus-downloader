import requests as r
from src.proto.register_pb2 import Root

device_token = "3769f0e9b5ded3314e83ef926eda01ce"  # Where to get this?
security_key = "45b9a9e654b01bd958d724f09a278b6f"  # Where to get this?
os = "android"
os_ver = "30"
app_ver = "53"


def get_secret():
    resp = r.put(
        f"https://jumpg-api.tokyo-cdn.com/api/register?device_token={device_token}&security_key={security_key}&os={os}&os_ver={os_ver}&app_ver={app_ver}")
    root = Root()
    root.ParseFromString(resp.content)

    return root.device.secret.token

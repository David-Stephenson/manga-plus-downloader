import os


def create_directory(base_name, create_new=False):
    dir_name = base_name
    counter = 1

    if create_new:
        while os.path.exists(dir_name):
            dir_name = f"{base_name}-{counter}"
            counter += 1

        os.makedirs(dir_name)

    return dir_name

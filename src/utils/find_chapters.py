import os
import requests
from src.proto.title_detailV2_pb2 import Root


def find_chapters(series_id):
    resp = requests.get(
        f"https://jumpg-api.tokyo-cdn.com/api/title_detailV2?title_id={series_id}&lang=eng&os=android&os_ver=30&app_ver=53&secret={os.environ['MANGA_PLUS_SECRET']}")

    root = Root()
    root.ParseFromString(resp.content)

    list = []
    for chapters in root.data.series.chapters:
        for chapter in chapters.freeInitialChapter:
            list.append({
                "id": chapter.chapterID,
                "chapter": chapter.chapter,
                "name": chapter.chapterName,
            })

        for chapter in chapters.appExclusiveChapter:
            list.append({
                "id": chapter.chapterID,
                "chapter": chapter.chapter,
                "name": chapter.chapterName,
            })

        for chapter in chapters.freeLatestChapter:
            list.append({
                "id": chapter.chapterID,
                "chapter": chapter.chapter,
                "name": chapter.chapterName,
            })

    return list


def chapter_from_id(series_id, chapter_id):
    chapters = find_chapters(series_id)
    for chapter in chapters:
        if int(chapter["id"]) == int(chapter_id):
            return chapter
    return None

import os
import requests
from tqdm import tqdm
from src.proto.manga_viewer_pb2 import Root
from src.utils.create_directory import create_directory
from src.utils.find_chapters import find_chapters, chapter_from_id
from src.utils.find_series import series_from_id


def downloader(mangaID, chapterID, quality):
    # Invalid quality
    if quality not in ("low", "high", "super"):
        print("Error: Invalid quality, use --help for more information.")
        return

    if quality == "super":
        quality = "super_high"

    # Find the series
    series = series_from_id(mangaID)
    print(f"{series['title']} by {series['author']}")

    # Create directory for the series
    series_dir = create_directory(series["title"], False)

    # Find the chapters in the series
    chapters = []
    if chapterID == 'latest' or chapterID == 'all':
        chapters_info = find_chapters(mangaID)
        if chapterID == 'latest':
            chapters.append(chapters_info[-1])
        else:
            chapters = chapters_info
    else:
        chapters.append(chapter_from_id(mangaID, chapterID))

    # Download the chapters
    for chap_num, chapter in enumerate(chapters, start=1):
        dir_name = create_directory(
            os.path.join(series_dir,  f"Chapter {chapter['chapter']}"), True)
        # print(f"Downloading {series['title']} chapter {chapter['chapter']}")

        resp = requests.get(
            f"https://jumpg-api.tokyo-cdn.com/api/manga_viewer?chapter_id={chapter['id']}&split=yes&img_quality={quality}&ticket_reading=&free_reading=&os=android&os_ver=30&app_ver=53&secret={os.environ['MANGA_PLUS_SECRET']}")

        root = Root()
        root.ParseFromString(resp.content)

        # Put all the images in a list
        img_list = [
            page.image.url for page in root.data.chapter.pages if page.image.url != ""]

        # Download images
        with tqdm(total=len(img_list), desc=f"Chapter {chap_num}/{len(chapters)}, Downloading chapter {chapter['chapter']} images", unit="image", leave=False, dynamic_ncols=True, bar_format="{l_bar}{bar}| {n_fmt}/{total_fmt}") as pbar:
            for i, url in enumerate(img_list, start=1):
                response = requests.get(url, stream=True)
                with open(os.path.join(dir_name, f'{i}.webp'), 'wb') as f:
                    for data in response.iter_content(1024):
                        f.write(data)
                pbar.update()  # Update progress after each image download

    print("Download complete")

import os
import requests
from src.proto.allV2_pb2 import Root


def find_series():
    resp = requests.get(
        f"https://jumpg-api.tokyo-cdn.com/api/title_list/all_v2?os=android&os_ver=30&app_ver=53&secret={os.environ['MANGA_PLUS_SECRET']}")

    root = Root()
    root.ParseFromString(resp.content)

    titles = []
    manga = root.parent.manga
    for series in manga.series:
        series_title = series.title
        for details in series.details:
            series_id = details.id
            lanaguage = details.language
            title = details.title
            author = details.author

            # Append the title information to the list
            titles.append({
                "id": series_id,
                "language": lanaguage,
                "series_title": series_title,
                "title": title,
                "author": author,
            })

    return titles


def series_from_id(series_id):
    series = find_series()
    for series in series:
        if series["id"] == series_id:
            return series
    return None

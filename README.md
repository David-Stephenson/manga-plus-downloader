# Manga Plus Downloader

Download manga from [MangaPlus](https://mangaplus.shueisha.co.jp/updates)

This Python CLI uses the Manga Plus mobile app API

For this project, I had to reverse engineer the Protobufs. I am continuing to reverse-engineer all the Protobufs for Manga Plus. To see more Manga Plus Protobufs, check out this [GitLab Repository](https://gitlab.com/David-Stephenson/manga-plus-protobuf)!

```
Usage: mp-dl [OPTIONS]

Options:
  --list           List all mangas or chapters. (Use --manga to list chapters
                   of a manga) (Default: list mangas)
  --download       Indicates a download action.
  --manga INTEGER  The ID of the maga to download.
  --chapter TEXT   The ID of the chapter to download. (ID, latest, all)
                   (Default: latest)
  --quality TEXT   The quality of the images to download. (super, high, low)
                   (Default: super)
  --help           Show this message and exit.
```

## Intallation
- Download this repository
- `pip install .`